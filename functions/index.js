const functions = require('firebase-functions');
const Telegraf = require('telegraf');
const axios  = require('axios');

let config = require('./../env.json')
if (Object.keys(functions.config()).length) {
    config = functions.config();
}
const bot = new Telegraf(config.service.telegram_key);

const weather_API_key = config.service.apiweatherstack_key;

bot.start((ctx) => ctx.reply('Welcome! Send me city, and i ask you actual weather'))
bot.help((ctx) => ctx.reply('Send me a sticker'))
bot.on('sticker', (ctx) => ctx.reply('👍'))
//bot.hears('hi', (ctx) => ctx.reply('Hey there'))
bot.on('text',(ctx) => {
    let query = ctx.update.message.text;
    let params = {
        access_key:weather_API_key,
        query:query
    };
    axios.get('http://api.weatherstack.com/current', {params})
        .then(response=>{
            return ctx.reply(
                `The current weather in ${query} is C: ${response.data.current.temperature}℃`
            );
        })
        .catch(error=>{
            return ctx.reply(query+'This city is not exists'+error+'...', error);
        })
})
bot.launch()


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest((request, res) => {

  let ans = "";
  let params = {
    access_key: weather_API_key,
    query:"Krasnoyarsk"
  };




  axios.get('http://api.weatherstack.com/current', {params})
      .then(response => {
        const apiResponse = response.data;
        ans = `Current temperature in ${apiResponse.location.name} is ${apiResponse.current.temperature}℃`;
        console.log("123456" + ans);
        res.send("Hello from Firebase:1234 " + ans);
      })
      .catch(error => {
    console.log(error);
        ans = error;
        res.send("Hello from Firebase:1234 " + ans);
  });
  //response.send("Hello from Firebase:1234 " + ans);

});


function sendRequest(city){
  let params = {
    access_key: weather_API_key,
    query:city
  };

  axios.get('https://api.weatherstack.com/current',{params})
      .then(response => {
        const apiResponse = response.data;
        let ans = 'В настоящий момент в городе '+apiResponse.location.name+" установилась температура "+apiResponse.current.temperature+" ℃";
        console.log(ans);
        return ans;

      })
      .catch(error =>{
        console.log(error);
        return error;
      })
}
